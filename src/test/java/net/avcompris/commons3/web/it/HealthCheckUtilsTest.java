package net.avcompris.commons3.web.it;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;
import static net.avcompris.commons3.web.it.DummyDbTables.DUMMY_AUTH;
import static net.avcompris.commons3.web.it.DummyDbTables.DUMMY_AUTH_SESSIONS;
import static net.avcompris.commons3.web.it.DummyDbTables.DUMMY_CORRELATIONS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.dao.HealthCheckDb.RuntimeDbColumn;
import net.avcompris.commons3.dao.HealthCheckDb.RuntimeDbTable;
import net.avcompris.commons3.dao.impl.HealthCheckDbUtils;
import net.avcompris.commons3.web.MutableHealthCheck;

public class HealthCheckUtilsTest {

	@Test
	public void testOK_DUMMY_CORRELATIONS() throws Exception {

		final String tableName = ensureDbTableName(DUMMY_CORRELATIONS);

		final MutableHealthCheck healthCheck = instantiate(MutableHealthCheck.class) //
				.setOk(true) //
				.setComponentName(HealthCheckUtilsTest.class.getSimpleName());

		HealthCheckDbUtils.populateRuntimeDbStatus( //
				healthCheck, //
				getDataSource(), //
				getTestProperty("rds.tableNamePrefix"), //
				DUMMY_CORRELATIONS);

		assertEquals(true, healthCheck.isOk());
		assertEquals("HealthCheckUtilsTest", healthCheck.getComponentName());
		assertNull(healthCheck.getErrors());

		assertEquals(true, healthCheck.getRuntimeDbStatus().isOk());
		assertEquals(1, healthCheck.getRuntimeDbStatus().getTables().length);

		final RuntimeDbTable dbTable0 = healthCheck.getRuntimeDbStatus().getTables()[0];
		assertEquals(true, dbTable0.isOk());
		assertEquals(tableName, dbTable0.getRuntimeName());
		assertEquals(DUMMY_CORRELATIONS.name(), dbTable0.getCompileName());
		assertTrue(dbTable0.getExistsInRuntimeDb());

		assertEquals(2, dbTable0.getColumns().length);

		final RuntimeDbColumn dbColumn0_0 = dbTable0.getColumns()[0];
		assertEquals(true, dbColumn0_0.isOk());
		assertEquals("correlation_id", dbColumn0_0.getName());
		assertEquals("VARCHAR(255) NOT NULL PRIMARY KEY", dbColumn0_0.getRuntimeLiteral());
		assertEquals("VARCHAR(255) NOT NULL PRIMARY KEY", dbColumn0_0.getCompileLiteral());

		final RuntimeDbColumn dbColumn0_1 = dbTable0.getColumns()[1];
		assertEquals(true, dbColumn0_1.isOk());
		assertEquals("created_at", dbColumn0_1.getName());
		assertEquals("TIMESTAMPTZ NOT NULL", dbColumn0_1.getRuntimeLiteral());
		assertEquals("TIMESTAMPTZ NOT NULL", dbColumn0_1.getCompileLiteral());
	}

	@Test
	public void testOK_DUMMY_AUTH() throws Exception {

		final String tableName = ensureDbTableName(DUMMY_AUTH);

		final MutableHealthCheck healthCheck = instantiate(MutableHealthCheck.class) //
				.setOk(true) //
				.setComponentName(HealthCheckUtilsTest.class.getSimpleName());

		HealthCheckDbUtils.populateRuntimeDbStatus( //
				healthCheck, //
				getDataSource(), //
				getTestProperty("rds.tableNamePrefix"), //
				DUMMY_AUTH);

		assertEquals(true, healthCheck.isOk());
		assertEquals("HealthCheckUtilsTest", healthCheck.getComponentName());
		assertNull(healthCheck.getErrors());

		assertEquals(true, healthCheck.getRuntimeDbStatus().isOk());
		assertEquals(2, healthCheck.getRuntimeDbStatus().getTables().length);

		final RuntimeDbTable dbTable0 = healthCheck.getRuntimeDbStatus().getTables()[0];
		assertEquals(true, dbTable0.isOk());
		assertEquals(tableName, dbTable0.getRuntimeName());
		assertEquals(DUMMY_AUTH.name(), dbTable0.getCompileName());

		final RuntimeDbTable dbTable1 = healthCheck.getRuntimeDbStatus().getTables()[1];
		assertEquals(true, dbTable1.isOk());
		assertEquals(tableName + "_sessions", dbTable1.getRuntimeName());
		assertEquals(DUMMY_AUTH_SESSIONS.name(), dbTable1.getCompileName());
	}

	@Test
	public void testOK_DUMMY_CORRELATIONS_no_table() throws Exception {

		final String tableName = ensureDbTableName(DUMMY_CORRELATIONS);

		try (Connection cxn = getDataSource().getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("DROP TABLE " + tableName)) {

				pstmt.executeUpdate();
			}
		}

		final MutableHealthCheck healthCheck = instantiate(MutableHealthCheck.class) //
				.setOk(true) //
				.setComponentName(HealthCheckUtilsTest.class.getSimpleName());

		HealthCheckDbUtils.populateRuntimeDbStatus( //
				healthCheck, //
				getDataSource(), //
				getTestProperty("rds.tableNamePrefix"), //
				DUMMY_CORRELATIONS);

		assertEquals(false, healthCheck.isOk());
		assertEquals("HealthCheckUtilsTest", healthCheck.getComponentName());
		assertEquals(1, healthCheck.getErrors().length);

		assertEquals(false, healthCheck.getRuntimeDbStatus().isOk());
		assertEquals(1, healthCheck.getRuntimeDbStatus().getTables().length);

		final RuntimeDbTable dbTable0 = healthCheck.getRuntimeDbStatus().getTables()[0];
		assertEquals(false, dbTable0.isOk());
		assertEquals(tableName, dbTable0.getRuntimeName());
		assertEquals(DUMMY_CORRELATIONS.name(), dbTable0.getCompileName());
		assertFalse(dbTable0.getExistsInRuntimeDb());

		assertEquals(2, dbTable0.getColumns().length);
		assertEquals(false, dbTable0.getColumns()[0].isOk());
		assertEquals("correlation_id", dbTable0.getColumns()[0].getName());
		assertNotNull(dbTable0.getColumns()[0].getCompileLiteral());
		assertNull(dbTable0.getColumns()[0].getRuntimeLiteral());
	}
}
