package net.avcompris.commons3.web.it;

import static net.avcompris.commons3.dao.DbTable.Type.INTEGER;
import static net.avcompris.commons3.dao.DbTable.Type.TIMESTAMP_WITH_TIMEZONE;
import static net.avcompris.commons3.dao.DbTable.Type.VARCHAR;
import static net.avcompris.commons3.dao.DbTablesUtils.column;

import java.util.Arrays;

import net.avcompris.commons3.dao.DbTable;
import net.avcompris.commons3.dao.DbTablesUtils.Column;

enum DummyDbTables implements DbTable {

	DUMMY_CORRELATIONS( //
			column("correlation_id") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("created_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build()),

	DUMMY_AUTH( //
			column("username") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("age") //
					.type(INTEGER) //
					.notNull() //
					.index() //
					.build()),

	DUMMY_AUTH_SESSIONS( //
			column("username") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("created_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.index() //
					.build());

	private final Column[] columns;

	private DummyDbTables(final Column... columns) {

		this.columns = columns;
	}

	@Override
	public Column[] columns() {

		return Arrays.copyOf(columns, columns.length);
	}
}
